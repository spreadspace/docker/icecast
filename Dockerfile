FROM registry.gitlab.com/spreadspace/docker/foundation/debian:bookworm

RUN set -x \
    && apt-get update -q \
    && apt-get install -y -q icecast2 \
    && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

USER app
